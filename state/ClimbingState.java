package state;

import java.awt.event.KeyEvent;
import objects.Player;

/**
 *
 * @author Frans
 */
class ClimbingState implements State{
    @Override
    public void handleAction(Player player, KeyEvent e) {
        player.setVelX(0);
        player.setVelY(0);
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            player.setVelY(-5);
        }
        else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            player.setVelY(5);
        }
    }

    @Override
    public void handleKeyRelease(Player player, KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_UP) {
            player.setVelY(0);
        }
    }

    @Override
    public void update(Player hero) {

    }
}
