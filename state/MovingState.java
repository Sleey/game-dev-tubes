/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import java.awt.event.KeyEvent;
import objects.Player;

/**
 *
 * @author Frans
 */
public class MovingState implements State{

    @Override
    public void handleAction(Player player, KeyEvent e) {
        player.onTopLadder = false;
        player.onBottomLadder = false;
        
        
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            player.setVelX(-player.moveSpeed);
            player.setDirection(0);
        }
        else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            player.setVelX(player.moveSpeed);
            player.setDirection(1);
        }
        else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            player.state = State.jumping;
            player.state.handleAction(player, e);
        }
        else if (e.getKeyCode() == KeyEvent.VK_Z) {
            player.fire();
            
        }
    }

    @Override
    public void handleKeyRelease(Player player, KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_RIGHT) {
            player.setVelX(0);
            player.state = State.standing;
        }
    }

    @Override
    public void update(Player player) {
        
    }
    
}
