/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import core.Assets;
import core.GameObject;
import core.ObjectId;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.LinkedList;

/**
 *
 * @author Lenovo
 */
public class DrawnObject extends GameObject{

    public DrawnObject(ObjectId id, float x, float y, int w, int h) {
        super(id, x, y, w, h);
    }

    @Override
    public void tick(LinkedList<GameObject> objects) {
    }

    @Override
    public void render(Graphics2D g) {
        if(id == ObjectId.TYPE_COINS){
            g.drawImage(Assets.coins, null, (int)x, (int)y);
        }

    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle((int)x, (int)y, w, h);
    }
    
}
