package core;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

public class Handler {
    public LinkedList<GameObject> objects;
    private GameObject tempGameObject;
    public Set<Integer> keys;
    private Iterator<GameObject> iterator;
    
    public Handler() {
        objects = new LinkedList<>();
        keys = new HashSet<Integer>();
    }
    
    public synchronized void tick() {

        LinkedList<GameObject> temp = new LinkedList<>(objects);

        for(GameObject g : temp) {
            g.tick(objects);
        }
    }
    
    public synchronized void render(Graphics2D g) {
        iterator = objects.iterator();
        while (iterator.hasNext()) {
            GameObject o = iterator.next();
            o.render(g);
        }
    }
    
    public synchronized void addObject(GameObject object) {
        objects.add(object);
    }
    
    public synchronized void removeObject(GameObject object) {
        objects.remove(object);
    }
    
    public synchronized void addKey(int key) {
        keys.add(key);
    }
    
    public synchronized void removeKey(int key) {
        keys.remove(key);
    }
}
