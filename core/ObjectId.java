package core;

public enum ObjectId {
    Player(),
    Enemy(),
    Boss(),
    Enemy_Bullet(),
    Bullet(),
    TYPE_GROUND(), 
    TYPE_EXIT(), 
    TYPE_LADDER(), 
    Spike(),
    Ladder(),
    TYPE_COINS(),
    Shop(),
    BUTTON();
}
