/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.net.URL;
import java.util.LinkedList;

/**
 *
 * @author Lenovo
 */
public class Sound{

    AudioClip click;
    public enum SoundState{
        PLAY, LOOP, STOP;
    }
    SoundState currentSoundState;
    public Sound(String url) {
        URL urlClick = Sound.class.getResource("/assets/sound/"+url);
        click = Applet.newAudioClip(urlClick);
    }

    public AudioClip getClick() {
        return click;
    }

    public void setClick(AudioClip click) {
        this.click = click;
    }
    
    public void play() throws InterruptedException{
        if(currentSoundState != SoundState.PLAY){
            click.play();
            
            currentSoundState = SoundState.PLAY;
            Thread.sleep(2000);
            click.stop();
        }
        
    }
    
    public void loop(){
        if(currentSoundState != SoundState.LOOP){
            click.loop();
            currentSoundState = SoundState.LOOP;
        }
    }
    
    public void stop(){
        click.stop();
        currentSoundState = SoundState.STOP;
        
    }
}
