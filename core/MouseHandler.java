package core;

import main.Game;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 *
 * @author Christian
 */
public class MouseHandler extends MouseAdapter{
//    public ObjectHandler handler;
    
    @Override
    public void mouseReleased(MouseEvent e) {
        if(Game.getInstance().menu.btnPlay.isHover()){
            Game.getInstance().playGame();
            
        }
        if(Game.getInstance().menu.btnExit.isHover()){
            System.exit(0);
        }
        
    }
    
}
